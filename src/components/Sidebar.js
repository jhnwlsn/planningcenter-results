import React from "react"

class Address extends React.Component {
  render() {
    return (
      <li><strong>{this.props.label}</strong><br/>{this.props.detail}</li>
    )
  }
}

class Email extends React.Component {
  render() {
    return (
      <li><strong>{this.props.label}</strong><a href={this.props.detail}>{this.props.detail}</a></li>
    )
  }
}

class Phone extends React.Component {
  render() {
    return (
      <li><strong>{this.props.label}</strong>{this.props.detail}</li>
    )
  }
}

class Sidebar extends React.Component {
  constructor(props) {
    super(props);
    this.closeSidebar = this.closeSidebar.bind(this);
  }
  closeSidebar() {
    this.props.closeSidebar();
  }
  render() {
    return (
      <div className="Detail Window-Sidebar">
        <a className="Detail-Close" onClick={this.closeSidebar}>&times;</a>
        <img className="Detail-Avatar" src="http://fillmurray.com/300/300" alt="temporary placeholder avatar" />
        <h2 className="Detail-Name">{this.props.details.name}<span>28</span></h2>
        <p className="Detail-Label Detail-Label--Numbers"><i className="material-icons">smartphone</i>Numbers</p>
        <ul className="Detail-List">
          { Object.keys(this.props.details.phone).map((key, i) => <Phone key={i} label={key} detail={this.props.details.phone[key]} />) }
        </ul>
        <p className="Detail-Label Detail-Label--Emails"><i className="material-icons">desktop_mac</i>Emails</p>
        <ul className="Detail-List">
          { Object.keys(this.props.details.email).map((key, i) => <Email key={i} label={key} detail={this.props.details.email[key]} />) }
        </ul>
        <p className="Detail-Label Detail-Label--Addresses"><i className="material-icons">email</i>Addresses</p>
        <ul className="Detail-List Detail-List--Addresses">
          { Object.keys(this.props.details.address).map((key, i) => <Address key={i} label={key} detail={this.props.details.address[key]} />) }
        </ul>
      </div>
    )
  }
}

export default Sidebar