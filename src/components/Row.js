import React from 'react'

class Row extends React.Component {
  constructor(props) {
    super(props);
    this.selectPerson = this.selectPerson.bind(this);
  }
  selectPerson(data) {
    this.props.selectPerson(data);
  }
  render() {
    return (
      <div className="Row" onClick={this.selectPerson.bind(null, this.props.details)}>
        <img className="Row-Avatar" src="http://fillmurray.com/150/150" alt="temporary placeholder avatar" />
        <p className="Row-Name">{this.props.details.name}</p>
        <ul className="Row-Details">
          <li>Age: {this.props.details.age}</li>
          <li><i className="material-icons">smartphone</i>{Object.keys(this.props.details.phone).length}</li>
          <li><i className="material-icons">desktop_mac</i>{Object.keys(this.props.details.email).length}</li>
          <li><i className="material-icons">email</i>{Object.keys(this.props.details.address).length}</li>
        </ul>
      </div>
    )
  }
}

export default Row