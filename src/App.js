import React, { Component } from 'react';

import Data from './data'
import Row from './components/Row'
import Sidebar from './components/Sidebar'
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      selected: {
        "name": "Benedict Cumberbatch",
        "age": "28",
        "phone": {
          "cell": "+1 (123) 456-7890",
          "home": "+1 (123) 456-7890",
          "work": "+1 (123) 456-7890",
          "other": "+1 (123) 456-7890"
        },
        "email": {
          "home": "",
          "work": "",
          "other": ""
        },
        "address": {
          "home": "",
          "work": "",
          "other": ""
        }
      },
      sidebar: false
    }
    this.closeSidebar = this.closeSidebar.bind(this);
    this.selectPerson = this.selectPerson.bind(this);
  }
  closeSidebar() {
    this.setState({sidebar: false});
  }
  selectPerson(data) {
    this.setState({
      selected: data,
      sidebar: true
    });
  }
  render() {
    return (
      <div className="Wrapper">
        <div className="Table">
          <header className="Table-Header">
            First &amp; Last Name
            <div>
              <ul className="Table-Header-Details">
                <li><i className="material-icons">smartphone</i>Numbers</li>
                <li><i className="material-icons">desktop_mac</i>Emails</li>
                <li><i className="material-icons">email</i>Addresses</li>
              </ul>
            </div>
          </header>
          <div className={"Window " + (this.state.sidebar ? "is-open" : null)}>
            <div className="Window-Inner">
              {Data.map((data, i) => <Row key={i} details={data} selectPerson={this.selectPerson} />)}
            </div>
            <Sidebar closeSidebar={this.closeSidebar} details={this.state.selected} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
